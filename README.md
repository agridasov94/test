<p align="center">
    <h2 align="center">Nginx Configuration for Frontend</h2>
    <br>
</p>
<pre>
server {
        #charset utf-8;
        #client_max_body_size 128M;

        listen 80;
        #listen [::]:80 default_server;

        server_name kivork-test.test www.kivork-test.test;
        root        /var/www/alex_gridasov/frontend/web/;
        index       index.php;

        access_log  /var/www/alex_gridasov/frontend/runtime/logs/frontend-access.log;
        error_log   /var/www/alex_gridasov/frontend/runtime/logs/frontend-error.log;

        location / {
            # Redirect everything that isn't a real file to index.php
            try_files $uri $uri/ /index.php$is_args$args;
        }

        # uncomment to avoid processing of calls to non-existing static files by Yii
        #location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
        #    try_files $uri =404;
        #}
        #error_page 404 /404.html;

        # deny accessing php files for the /assets directory
        location ~ ^/assets/.*\.php$ {
            deny all;
        }

        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            #fastcgi_pass 127.0.0.1:9000;
            fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
            try_files $uri =404;
        }
    
        location ~* /\. {
            deny all;
        }
    }
</pre>

<p align="center">
    <h2 align="center">Nginx Configuration for Backend</h2>
    <br>
</p>
<pre>
server {
        #charset utf-8;
        #client_max_body_size 128M;

        listen 80;
        #listen [::]:80 default_server;

        server_name back.kivork-test.test www.back.kivork-test.test;
        root        /var/www/alex_gridasov/backend/web/;
        index       index.php;

        access_log  /var/www/alex_gridasov/backend/runtime/logs/frontend-access.log;
        error_log   /var/www/alex_gridasov/backend/runtime/logs/frontend-error.log;

        location / {
            # Redirect everything that isn't a real file to index.php
            try_files $uri $uri/ /index.php$is_args$args;
        }

        # uncomment to avoid processing of calls to non-existing static files by Yii
        #location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
        #    try_files $uri =404;
        #}
        #error_page 404 /404.html;

        # deny accessing php files for the /assets directory
        location ~ ^/assets/.*\.php$ {
            deny all;
        }

        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            #fastcgi_pass 127.0.0.1:9000;
            fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
            try_files $uri =404;
        }
    
        location ~* /\. {
            deny all;
        }
    }
</pre>