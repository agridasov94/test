<?php

use common\bootstrap\InitEventDispatcher;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'mailer' => [
			'class' => \yii\swiftmailer\Mailer::class,
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'smtp.gmail.com',
				'username' => 'alex.grub@techork.com',
				'password' => 'rhxkclrcnaefyvje',
				'port' => 465,
				'encryption' => 'SSL'
			]
		]
    ],
	'bootstrap' => [
		InitEventDispatcher::class
	]

];
