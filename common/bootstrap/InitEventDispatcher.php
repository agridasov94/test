<?php

namespace common\bootstrap;

use common\bootstrap\event\CreateCountryEvent;
use common\bootstrap\event\CreateCountryEventListener;
use common\bootstrap\event\EventDispatcher;
use common\bootstrap\event\SignUpVerificationEmailEvent;
use common\bootstrap\event\SignUpVerificationEmailEventListener;
use common\bootstrap\event\SimpleEventDispatcher;
use yii\di\Container;

class InitEventDispatcher implements \yii\base\BootstrapInterface
{
	public function bootstrap($app)
	{
		$container = \Yii::$container;

		$container->setSingleton(EventDispatcher::class, SimpleEventDispatcher::class);

		$container->setSingleton(SimpleEventDispatcher::class, static function () {
			return new SimpleEventDispatcher([
				CreateCountryEvent::class => [CreateCountryEventListener::class],
				SignUpVerificationEmailEvent::class => [SignUpVerificationEmailEventListener::class]
			]);
		});
	}
}