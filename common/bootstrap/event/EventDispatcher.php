<?php


namespace common\bootstrap\event;

interface EventDispatcher
{
	public function dispatchAll(array $events): void;
	public function dispatch($event): void;
}