<?php

namespace common\bootstrap\event;

use yii\base\Event;

trait EventTrait
{
	private $events;

	public function addEvent(Event $event): void
	{
		$this->events[] = $event;
	}

	public function releaseEvents(): array {
		$events = $this->events;
		$this->events = [];
		return $events;
	}
}
