<?php

namespace common\bootstrap\event;

class SignUpVerificationEmailEventListener
{
	public function handle(SignUpVerificationEmailEvent $event): void
	{
		$user = $event->user;

		$result = $event->mailer->compose(
			['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
			['user' => $user])
			->setFrom('alex.grub@techork.com')
			->setTo($user->email)
			->setSubject('Confirmation of registration')
			->send();

		if (!$result) {
			throw new \RuntimeException('Error sent sign up email');
		}
	}
}