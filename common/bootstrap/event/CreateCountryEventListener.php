<?php


namespace common\bootstrap\event;


use app\models\Countries;

/**
 * Class CreateCountryEventListener
 * @package common\bootstrap\event
 *
 * @property Countries $country
 * @property
 */
class CreateCountryEventListener
{
	public function handle(CreateCountryEvent $event): void
	{
		$country = $event->country;
	}
}