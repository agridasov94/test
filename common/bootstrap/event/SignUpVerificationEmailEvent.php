<?php


namespace common\bootstrap\event;


use common\models\User;
use yii\base\Event;
use yii\mail\MailerInterface;

/**
 * Class SignUpVerificationEmailEvent
 * @package common\bootstrap\event
 *
 * @property User $user
 * @property MailerInterface $mailer
 */
class SignUpVerificationEmailEvent extends Event
{
	/**
	 * @var User
	 */
	public $user;

	/**
	 * @var MailerInterface
	 */
	public $mailer;

	/**
	 * SignUpVerificationEmailEvent constructor.
	 * @param User $user
	 * @param MailerInterface $mailer
	 * @param array $config
	 */
	public function __construct(User $user, MailerInterface $mailer, $config = [])
	{
		$this->user = $user;
		$this->mailer = $mailer;
		parent::__construct($config);
	}
}