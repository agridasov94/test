<?php


namespace common\bootstrap\event;

/**
 * Class SimpleEventDispatcher
 * @package common\bootstrap\event
 *
 * @property array $listeners
 * @property array $queue
 */
class SimpleEventDispatcher implements EventDispatcher
{
	private $listeners;

	private $queue;

	public function __construct($events)
	{
		$this->listeners = $events;
	}

	public function dispatch($event): void
	{
		$eventName = get_class($event);
		if (array_key_exists($eventName, $this->listeners)) {
			foreach ($this->listeners[$eventName] as $listener) {
				$listener = $this->resolveListener($listener);
				$listener($event);
			}
		}
	}

	public function dispatchAll(array $events): void
	{
		foreach ($events as $event) {
			$this->dispatch($event);
		}
	}

	public function addToQueue($event): void
	{
		$this->queue[] = $event;
	}

	public function release(): void
	{
		foreach ($this->queue as $event) {
			$this->dispatch($event);
		}
		$this->clearQueue();
	}

	public function clearQueue(): void
	{
		$this->queue = [];
	}

	private function resolveListener($listenerClass): callable
	{
		return [\Yii::createObject($listenerClass), 'handle'];
	}
}
