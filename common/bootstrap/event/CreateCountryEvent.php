<?php


namespace common\bootstrap\event;

use app\models\Countries;
use yii\base\Event;

/**
 * Class CreateCountryEvent
 * @package common\bootstrap\event
 *
 * @property Countries $country
 */
class CreateCountryEvent extends Event
{
	public $country;

	/**
	 * CreateCountryEvent constructor.
	 * @param Countries $country
	 * @param array $config
	 */
	public function __construct(Countries $country, array $config = [])
	{
		$this->country = $country;

		parent::__construct($config);
	}
}