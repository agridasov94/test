<?php


namespace common\services;

use common\bootstrap\event\SimpleEventDispatcher;

/**
 * Class TransactionManager
 * @package common\services
 *
 * @property SimpleEventDispatcher $dispatcher
 */
class TransactionManager
{
	private $dispatcher;

	public function __construct(SimpleEventDispatcher $simpleEventDispatcher)
	{
		$this->dispatcher = $simpleEventDispatcher;
	}

	public function wrap(callable $callback) {
		$result = null;
		$transaction = \Yii::$app->db->beginTransaction();
		try {
			$result = $callback();
			$this->dispatcher->release();
			$transaction->commit();
		} catch (\Throwable $e) {
			$transaction->rollBack();
			$this->dispatcher->clearQueue();
			throw $e;
		}
		return $result;
	}
}