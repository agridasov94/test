<?php

namespace common\services\auth;

use common\repository\auth\SignUpRepository;
use common\services\TransactionManager;
use frontend\models\SignupForm;
use Yii;

/**
 * Class SignUpService
 * @package common\services\auth
 *
 * @property TransactionManager $transactionManager
 * @property SignUpRepository $signUpRepository
 */
class SignUpService
{
	private $transactionManager;
	/**
	 * @var SignUpRepository
	 */
	private $signUpRepository;

	/**
	 * SignUpService constructor.
	 * @param TransactionManager $transactionManager
	 * @param SignUpRepository $signUpRepository
	 */
	public function __construct(TransactionManager $transactionManager, SignUpRepository $signUpRepository)
	{
		$this->transactionManager = $transactionManager;
		$this->signUpRepository = $signUpRepository;
	}

	/**
	 * @param SignupForm $signupForm
	 * @return int
	 * @throws \Throwable
	 */
	public function signUp(SignupForm $signupForm): int
	{
		return $this->transactionManager->wrap(function () use ($signupForm) {
			$user = $this->signUpRepository->signUp($signupForm);

			return $this->signUpRepository->save($user);
		});
	}
}