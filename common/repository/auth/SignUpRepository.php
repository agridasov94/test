<?php

namespace common\repository\auth;

use common\bootstrap\event\EventTrait;
use common\bootstrap\event\SignUpVerificationEmailEvent;
use common\bootstrap\event\SimpleEventDispatcher;
use common\dto\SignUpDTO;
use common\models\User;
use frontend\models\SignupForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class SignUpRepository
 * @package common\repository\auth
 *
 * @property SimpleEventDispatcher $eventDispatcher
 */
class SignUpRepository
{
	use EventTrait;

	private $eventDispatcher;

	/**
	 * SignUpRepository constructor.
	 * @param SimpleEventDispatcher $eventDispatcher
	 */
	public function __construct(SimpleEventDispatcher $eventDispatcher)
	{
		$this->eventDispatcher = $eventDispatcher;
	}

	/**
	 * @param SignupForm $signupForm
	 * @return User
	 * @throws \yii\base\Exception
	 * @throws \yii\base\InvalidConfigException
	 */
	public function signUp(SignupForm $signupForm): User
	{
		$user = User::signup((new SignUpDTO(
			$signupForm->username,
			$signupForm->email,
			Yii::$app->security->generatePasswordHash($signupForm->password),
			Yii::$app->security->generateRandomString(),
			Yii::$app->security->generateRandomString() . '_' . time()
		)));

		$this->eventDispatcher->addToQueue(Yii::createObject(SignUpVerificationEmailEvent::class, [$user, \Yii::$app->mailer]));

		return $user;
	}

	/**
	 * @param User $user
	 * @return int
	 */
	public function save(User $user)
	{
		if (!$user->save(false)) {
			throw new \RuntimeException('Saving user error');
		}
		return $user->id;
	}



}