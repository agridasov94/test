<?php

namespace common\dto;

/**
 * Class SignUpDTO
 * @package common\dto
 *
 * @property $username
 * @property $email
 * @property $passwordHash
 * @property $authKey
 * @property $emailVerificationToken
 */
class SignUpDTO
{
	public $username;
	public $email;
	public $passwordHash;
	public $authKey;
	public $emailVerificationToken;

	public function __construct(
		$username,
		$email,
		$passwordHash,
		$authKey,
		$emailVerificationToken
	)
	{
		$this->username = $username;
		$this->email = $email;
		$this->passwordHash = $passwordHash;
		$this->authKey = $authKey;
		$this->emailVerificationToken = $emailVerificationToken;
	}

}