<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ForecastSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel panel-default">
    <div class="panel-heading">
        Search
    </div>

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="panel-body row">

        <div class="col-xs-2">
                <?= $form->field($model, 'start')->widget('trntv\yii\datetime\DateTimeWidget', [
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientOptions' => [
                        'useCurrent' => false,
                        'defaultDate' => date('m.d.Y', strtotime('yesterday')),
                    ]
                ]) ?>
        </div>
        <div class="col-xs-2">
                <?= $form->field($model, 'end')->widget('trntv\yii\datetime\DateTimeWidget', [
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientOptions' => [
                        'useCurrent' => false,
                        'defaultDate' => date('m.d.Y', strtotime('today')),
                    ]
                ]) ?>
        </div>
        <div class="col-xs-2">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-success', 'style' => 'margin-top: 25px;']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
