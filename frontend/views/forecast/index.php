<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ForecastSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Forecasts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="forecast-index">

        <?php Pjax::begin(['enablePushState'=>false]); ?>
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],

                'Country:ntext:Country',
                'City:ntext:City',
                'maxTempCelsius:ntext:Max temperature',
                'minTempCelsius:ntext:Min temperature',
                'avgTempCelsius:ntext:Avg temperature',

                [
                    'class' => 'yii\grid\ActionColumn', 
                    'header' => 'Action',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return ButtonDropdown::widget([
                                'label' => 'Action',
                                'dropdown' => [
                                    'items' => [
                                        [ 
                                            'label' => 'History', 
                                            'url' => Url::to(['forecast/history', 'cityName' => $model->City]),
                                            'linkOptions' => [
                                                'data-pjax' => 0,
                                            ],
                                        ],
                                    ],
                                ],
                            ]);
                        }
                    ]
                ],

            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>
</div>
