<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Forecast */

$this->title = "History of " .  Html::encode($city->name) . " (" . Html::encode($city->country->name) . ")";
$this->params['breadcrumbs'][] = ['label' => 'Forecasts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container">
    <div class="panel panel-default">

        <?= Html::tag('div', Html::encode($this->title), ['class' => 'panel-heading']) ?>
        <?= Html::beginTag('div', ['class' => 'panel-body']) ?>
        <?php
            $prevDate = null;
            foreach ($model as $row) {
                $currentDate = \Yii::$app->formatter->asDateTime($row->when_created, 'yyyy-MM-dd');

                if ($prevDate !== null && $prevDate !== $currentDate) {
                    echo Html::endTag('div');
                }

                if ($prevDate == null || $prevDate !== $currentDate) {
                    echo Html::beginTag('div', ['class' => 'col-xs-3'] );

                    echo Html::tag('p', Html::tag('strong', \Yii::$app->formatter->asDate($row->when_created, 'long')));

                } 

                echo Html::tag('p', \Yii::$app->formatter->asDateTime($row->when_created, 'HH:mm:ss') . ' ' . $row->fahrenheiToCelsius($row->temperature));
                
                $prevDate = $currentDate;

            }
        ?>
        <?= Html::endTag('div') ?>


    </div>
</div>
