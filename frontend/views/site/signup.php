<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="site-signup">
                <h1><?= Html::encode($this->title) ?></h1>

                <p>Please fill out the following fields to signup:</p>

                <?php \yii\widgets\Pjax::begin(['timeout' => 2000, 'id' => 'signup-pjax', 'enablePushState' => false, 'enableReplaceState' => false]); ?>
                <?php $form = ActiveForm::begin(['id' => 'form-signup', 'enableClientValidation' => false, 'options' => [
                        'data-pjax' => true,
                ]]); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
