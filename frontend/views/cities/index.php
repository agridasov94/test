<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="cities-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Create Cities', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php Pjax::begin(['enablePushState'=>false]); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'country.name:ntext',
                'name:ntext',

                ['class' => 'yii\grid\ActionColumn', 'header' => 'Actions'],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>
</div>
