<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="container">
<p>
    Simple uploaded image form
</p>
<?php $form = ActiveForm::begin(['method' => 'post', 'action' => 'upload/upload']); ?>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="form-group">
            <div class="alert alert-success">
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>
</div>
