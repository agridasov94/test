<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>

<div class="container">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse',
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid'
        ]
    ]);
        $menuItems = [
            ['label' => 'Countries', 'url' => ['/countries']],
            ['label' => 'Cities', 'url' => ['/cities']],
            ['label' => 'Statistics', 'url' => ['/forecast/site/stats']],
            ['label' => 'Upload Files', 'url' => ['/upload']]
        ];
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => $menuItems,
        ]);

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                    ['label' => 'Sign-in', 'url' => ['/sign-in']],
                    ['label' => 'Sign-up', 'url' => ['/sign-up']]
            ]
        ]);
        NavBar::end();
        ?>

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

</div>
<div class="wrap">
    <div class="container">
        <div class="row">
            <?= $content ?>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
