<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forecast".
 *
 * @property int $id
 * @property int $city_id
 * @property double $temperature
 * @property int $when_created
 *
 * @property Cities $city
 */
class Forecast extends \yii\db\ActiveRecord
{
    public $Country;

    public $City;

    public $maxTemp;

    public $minTemp;

    public $avgTemp;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forecast';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'temperature', 'when_created'], 'required'],
            [['city_id', 'when_created'], 'default', 'value' => null],
            [['city_id', 'when_created'], 'integer'],
            [['temperature'], 'number'],
            [['city_id', 'when_created'], 'unique', 'targetAttribute' => ['city_id', 'when_created']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'temperature' => 'Temperature',
            'when_created' => 'When Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    public function getMaxTempCelsius() 
    {
        return $this->fahrenheiToCelsius($this->maxTemp);
    }

    public function getMinTempCelsius()
    {
        return $this->fahrenheiToCelsius($this->minTemp);
    }

    public function getAvgTempCelsius()
    {
        return $this->fahrenheiToCelsius($this->avgTemp);
    }

    /**
     * Method convert fahrenheit to celsius
     * 
     * @param int $fahrenheit
     * 
     * 
     */
    public function fahrenheiToCelsius (int $fahrenheit) 
    {
        $celsius = ($fahrenheit - 32) * 5/9;

        return (($celsius < 0) ? '' : '+' ) . number_format( ($fahrenheit - 32) * 5/9 ) . ' ' . html_entity_decode('&#8451;');
    }
}
