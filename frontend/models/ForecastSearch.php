<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Forecast;

/**
 * ForecastSearch represents the model behind the search form of `app\models\Forecast`.
 */
class ForecastSearch extends Forecast
{
    public $start;

    public $end;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'when_created'], 'integer'],
            [['temperature'], 'number'],
            [['when_created'], 'safe'],
            [['start', 'end' ], 'date', 'format' => 'php:d.m.Y']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Forecast::find()
        ->select([
            'countries.name as Country',
            'cities.name as City',
            'max(forecast.temperature) as maxTemp',
            'min(forecast.temperature) as minTemp',
            'AVG(forecast.temperature) as avgTemp'
        ])
        ->leftJoin('cities', 'cities.id = forecast.city_id')
        ->leftJoin('countries', 'countries.id = cities.country_id')
        ->groupBy('cities.name, countries.name');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes = [
            'Country' => [
                'asc' => ['Country' => SORT_ASC],

                'desc' => ['Country' => SORT_DESC],
            ],
            'City' => [
                'asc' => ['City' => SORT_ASC],

                'desc' => ['City' => SORT_DESC],
            ],
            'maxTempCelsius' => [
                'asc' => ['maxTemp' => SORT_ASC],

                'desc' => ['maxTemp' => SORT_DESC],
            ],
            'minTempCelsius' => [
                'asc' => ['minTemp' => SORT_ASC],

                'desc' => ['minTemp' => SORT_DESC],
            ],
            'avgTempCelsius' => [
                'asc' => ['avgTemp' => SORT_ASC],

                'desc' => ['avgTemp' => SORT_DESC],
            ],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['between', 'when_created', $this->start ? strtotime($this->start) : strtotime('yesterday'), $this->end ? strtotime($this->end) : strtotime('today')]);

        return $dataProvider;
    }
}
