<?php
namespace frontend\controllers;

use Yii;
use app\models\Forecast;
use app\models\ForecastSearch;
use app\models\Cities;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ForecastController implements the CRUD actions for Forecast model.
 */
class ForecastController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Forecast models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ForecastSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Forecast model.
     * @param string $cityName
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionHistory($cityName)
    {
        // get city
        $city = Cities::findOne(['name' => $cityName]);

        return $this->render('history', [
            'model' => $this->findModelByCity($city->id),
            'city' => $city
        ]);
    }

    /**
     * Finds the Forecast model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Forecast the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Forecast::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Forecast model based on city name.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param int $cityId
     * @return Forecast the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelByCity($cityId)
    {
        $model = Forecast::find()
        ->where(['city_id' => $cityId])
        ->orderBy(['when_created' => SORT_DESC])
        ->all();

        if (!empty($model)) return $model;

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
