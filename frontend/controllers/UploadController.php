<?php

namespace frontend\controllers;

use Yii;
use frontend\models\UploadForm;
use yii\web\UploadedFile;

class UploadController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $form = new UploadForm();

        return $this->render('index', [
            'model' => $form
        ]);
    }

    public function actionUploadImage()
    {
        if (Yii::$app->request->isPost) {

            $form = new UploadForm();

            $form->imageFile = UploadedFile::getInstance($form, 'imageFile');

            if ($form->upload()) {
                Yii::$app->session->setFlash('success', 'Image has been successfully uploaded;');
            }
        }

        return $this->redirect('upload/index');
    }

}
