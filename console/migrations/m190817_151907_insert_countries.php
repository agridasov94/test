<?php

use yii\db\Migration;

/**
 * Class m190817_151907_insert_countries
 */
class m190817_151907_insert_countries extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $countryList = require __DIR__ . '/../../vendor/umpirsky/country-list/data/en/country.php';

        foreach ($countryList as $country) {
            $this->insert('countries', ['name' => $country]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('countries');
    }
}
