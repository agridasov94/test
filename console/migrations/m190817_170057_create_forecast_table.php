<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%forecast}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%cities}}`
 */
class m190817_170057_create_forecast_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%forecast}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'temperature' => $this->float(1)->notNull(),
            'when_created' => $this->integer()->notNull(),
        ]);

        // creates index for column `city_id`
        $this->createIndex(
            '{{%idx-forecast-city_id}}',
            '{{%forecast}}',
            'city_id'
        );

        // add foreign key for table `{{%cities}}`
        $this->addForeignKey(
            '{{%fk-forecast-city_id}}',
            '{{%forecast}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            '{{%idx-unique-forecast-cities}}', 
            '{{%forecast}}', 
            'city_id, when_created', 
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%cities}}`
        $this->dropForeignKey(
            '{{%fk-forecast-city_id}}',
            '{{%forecast}}'
        );

        // drops index for column `city_id`
        $this->dropIndex(
            '{{%idx-forecast-city_id}}',
            '{{%forecast}}'
        );

        $this->dropIndex(
            '{{%idx-unique-forecast-cities}}', 
            '{{%forecast}}'
        );

        $this->dropTable('{{%forecast}}');
    }
}
