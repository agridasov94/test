<?php

use yii\db\Migration;

/**
 * Class m191203_082802_edit_default_value_of_tbl_user_column_status
 */
class m191203_082802_edit_default_value_of_tbl_user_column_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('{{%user}}', 'status', $this->tinyInteger(3)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
