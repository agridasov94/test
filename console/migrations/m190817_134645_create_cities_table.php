<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cities}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%countries}}`
 */
class m190817_134645_create_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->text(),
        ]);

        // creates index for column `country_id`
        $this->createIndex(
            '{{%idx-cities-country_id}}',
            '{{%cities}}',
            'country_id'
        );

        // add foreign key for table `{{%countries}}`
        $this->addForeignKey(
            '{{%fk-cities-country_id}}',
            '{{%cities}}',
            'country_id',
            '{{%countries}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            '{{%idx-unique-cities}}', 
            '{{%cities}}', 
            'country_id, name', 
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%countries}}`
        $this->dropForeignKey(
            '{{%fk-cities-country_id}}',
            '{{%cities}}'
        );

        // drops index for column `country_id`
        $this->dropIndex(
            '{{%idx-cities-country_id}}',
            '{{%cities}}'
        );

        $this->dropIndex(
            '{{%idx-unique-cities}}', 
            '{{%cities}}' 
        );

        $this->dropTable('{{%cities}}');
    }
}
