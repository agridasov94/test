<?php

namespace console\controllers;

use app\models\Cities;
use app\models\Forecast;
use DOMDocument;
use DOMElement;
use yii\base\Exception;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Command receives the weather forecast at the specified url address and saves the data without duplicates in the database
 */
class JobController extends Controller
{

    /**
     * @var array URL sources with xml content
     */
    private $sourceURLs = array(
        'http://quiz.dev.travelinsides.com/forecast/api/getForecast?start=19.08.2019&end=20.08.2019&city=Moscow',
        // 'http://quiz.dev.travelinsides.com/forecast/api/getForecast?start=01.03.2018&end=31.12.2018&city=Chicago',
        // 'http://quiz.dev.travelinsides.com/forecast/api/getForecast?start=01.07.2018&end=21.07.2018&city=Houston',
    );

    /**
     * @var array XML content
     */
    private $domXML = [];

    /**
     * @var array
     */
    private $requiredTags = [
        'city',
        'temperature',
        'ts'
    ];

    /**
     * @var int
     */
    private $countSavedRecords = 0;

    /**
     * @var string
     */
    private $errorLogPath = __DIR__ . '/../runtime/logs/job_commands.log';

    /**
     * @var bool 
     */
    private $errorExists = false;

    /**
     * @return int ExitCode 
     */
    public function actionIndex()
    {
        try {

            $this->getForecast()->getForecastXML();
            
        } catch (Exception $e) {
            $this->storeError( $e->getMessage() . '; Code: ' . $e->getCode() . '; On Line: ' . $e->getLine() . '; In File: ' . $e->getFile());
        }

        $this->printFinalMessage();
        
        return ExitCode::OK;
    }

    /**
     * Method for retrieving XML content using source URL
     * 
     * @return object $this
     */
    private function getForecast()
    {
        $multi = curl_multi_init();
        $channels = array();
         
        foreach ($this->sourceURLs as $url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         
            curl_multi_add_handle($multi, $ch);
         
            $channels[$url] = $ch;
        }
         
        $active = null;
        do {
            $status = curl_multi_exec($multi, $active);
            curl_multi_select($multi);
        } while ($active > 0 && $status === CURLM_OK);

        foreach ($channels as $url => $channel) {
            $this->domXML[$url] = curl_multi_getcontent($channel);
            curl_multi_remove_handle($multi, $channel);
        }

        curl_multi_close($multi);

        return $this;
    }

	/**
	 * Method parse xml content
	 *
	 * @return void
	 * @throws Exception
	 */
    private function getForecastXML()
    {
        if (empty($this->domXML)) throw new Exception('xml is not uploaded' , 601);

        foreach ($this->domXML as $url => $xml) {
            $dom = new DOMDocument();
            $dom->loadXML($xml);

            $documentElement = $dom->documentElement;

            if ($documentElement->tagName === 'errors') {
                $this->storeErrorFromXML($documentElement, $url);
                continue;
            }

			foreach ($documentElement->getElementsByTagName('row') as $row) {
                if ( !$this->checkRequiredTags($row, $url) ) {$this->storeError('Node doesnt have all requred tags;', $url); break;}

                $forecastData = [];

                foreach ($row->childNodes as $child) {
                    $forecastData[$child->tagName] = $child->nodeValue;
                }

                $this->saveForecast($forecastData, $url);
                // if something went wrong then exit from the loop
                // if ( !$this->saveForecast($forecastData, $url) ) break;  
            } 
        }
    }

    /**
     * Method for saving errors to a log file
     * 
     * @param string $message
     * 
     * @param string $url
     */
    private function storeError(string $message, string $url = null)
    {
        error_log('Error: ' . $message . '; URL: ' . $url . "\n", 3, $this->errorLogPath);

        $this->errorExists = true;
    }

	/**
	 * Method for checking errors in the xml file
	 *
	 * @param DOMElement $dom
	 *
	 * @param string $url
	 *
	 * @return void
	 */
    private function storeErrorFromXML(\DOMElement $dom, string $url)
    {
        foreach ($dom->childNodes as $item) 
        {
            $this->storeError($item->nodeValue, $url);
        }
    }

    /**
     * @param DOMElement $dom
     * 
     * @param string $url
     * 
     * @return bool
     */
    private function checkRequiredTags(\DOMElement $dom, string $url)
    {
        foreach ($this->requiredTags as $tag) {
            if ($dom->getElementsByTagName($tag)->length == 0) return false;
        }

        return true;
    }

    /**
     * @return void
     */
    private function printFinalMessage()
    {
        if ($this->errorExists) print "Errors occurred during program execution, see them in the log file;" . PHP_EOL;

        print "Program completed successfully, number of records saved: " . $this->countSavedRecords . PHP_EOL;
    }

    /**
     * Method for saving forecasts
     * 
     * @return bool
     */
    private function saveForecast(array $forecastData, string $url)
    {
        $city = Cities::findOne([
            'name' => $forecastData['city']
        ]);

        if ($city === null) {
            $this->storeError("City: {$forecastData['city']} not found", $url);
            return false;
        }

        $forecast = new Forecast();
        $forecast->city_id = $city->id;
        $forecast->temperature = $forecastData['temperature'];
        $forecast->when_created = $forecastData['ts'];

        if (!$forecast->save()) {
            foreach ($forecast->getErrorSummary(true) as $error) {
                $this->storeError($error, $url); 
                return false;
            }
        } 

        $this->countSavedRecords++;

        return true;
    }
}
