<?php


namespace console\controllers;


use yii\console\Controller;
use yii\swiftmailer\Mailer;

class TestController extends Controller
{
	public function actionSendEmail($emailTo) {
		try {
			$result = \Yii::$app->mailer->compose()
				->setFrom('alex.grub@techork.com')
				->setTo($emailTo)
				->setSubject('Test mail subject')
				->setTextBody('Test text body')
				->setHtmlBody('<h1>Hello World</h1>')
				->send();

			var_dump(\Yii::$app->mailer->getTransport());

			var_dump($result);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
}